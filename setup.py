from setuptools import setup

setup(
    name='bestellverwaltung',
    version='1.0',
    long_description=__doc__,
    packages=['bestellverwaltung'],
    include_package_data=True,
    zip_safe=False,
    install_requires=['Flask', 'peewee', 'uwsgi', 'redis']
)
