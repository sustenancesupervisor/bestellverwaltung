FROM python:3.8
LABEL maintainer="cozyGalvinism <reallifejunkies@googlemail.com>"

RUN apt update && \
    apt install -y redis-tools

COPY ./ /opt/bestellverwaltung
RUN mkdir /opt/bestellverwaltung/database
WORKDIR /opt/bestellverwaltung
RUN ls
RUN python setup.py install

EXPOSE 5000

CMD ["uwsgi", "--http", "0.0.0.0:5000", "--master", "--processes", "4", "--module", "bestellverwaltung.bestellverwaltung:app", "--enable-threads"]