import os
import peewee

if not os.path.exists('database'):
    os.mkdir('database')
db = peewee.SqliteDatabase(os.getenv('SQLITE_PATH', 'database/db.sqlite3'), pragmas={'foreign_keys': 1})

class BaseModel(peewee.Model):
    class Meta:
        database = db

class User(BaseModel):
    id = peewee.IntegerField(primary_key=True)
    first_name = peewee.CharField()
    last_name = peewee.CharField()
    email = peewee.CharField()

class Food(BaseModel):
    Id = peewee.IntegerField(primary_key=True)
    Name = peewee.CharField()
    Preis = peewee.DoubleField()

class Order(BaseModel):
    user = peewee.ForeignKeyField(User, on_delete='SET NULL', backref='orders', null=True)
    delivered = peewee.BooleanField(default=False)

    def total_price(self):
        total = 0.0
        for pos in self.positions:
            total = total + pos.total_price
        return total
    
    def mark_delivered(self):
        if not self.delivered:
            self.delivered = True
        return True

class OrderPosition(BaseModel):
    order = peewee.ForeignKeyField(Order, on_delete='SET NULL', backref='positions')
    food = peewee.ForeignKeyField(Food, on_delete='SET NULL', backref='+')
    quantity = peewee.IntegerField()

    @property
    def total_price(self):
        return self.quantity * self.food.price

def connect():
    db.connect()
    db.create_tables([User, Food, Order, OrderPosition])

def close():
    db.close()