import sys
import logging
import signal
from flask import Flask, jsonify, request
from playhouse.shortcuts import (dict_to_model, model_to_dict,
                                 update_model_from_dict)

import bestellverwaltung.database
from bestellverwaltung.database import Order, User, Food, OrderPosition
from bestellverwaltung.database import db as database
from bestellverwaltung.broker import RedisBroker

app = Flask(__name__)

logger = app.logger # type: logging.Logger
logger.setLevel(logging.DEBUG)
logger.info('Initialize database...')
bestellverwaltung.database.connect()
broker = RedisBroker(database, logger)
broker.subscribe_all()
broker.start()

app.config['JSON_AS_ASCII'] = False

@app.route('/orders', methods=['GET'])
def orders():
    all_orders = [model_to_dict(mod, backrefs=True) for mod in Order.select()]
    return jsonify(all_orders)

@app.route('/orders/<int:order_id>', methods=['GET'])
def get_order(order_id):
    try:
        order = Order.select().where(Order.id == order_id).get()
        order_dict = model_to_dict(order, backrefs=True)
        return jsonify(order_dict)
    except Order.DoesNotExist:
        return jsonify({'result': False, 'message': 'No order with specified id!'})

@app.route('/debug', methods=['GET'])
def debug_req():
    user = User(id=1, first_name="Barry B", last_name="Benson", email="bee@beemail.com")
    user.save(force_insert=True)
    food = Food(id=1, Name="Beezza", Preis=2.99)
    food.save(force_insert=True)
    return jsonify({"result": True})

@app.route('/orders', methods=['POST'])
def create_order():
    print('Data: ' + request.data.decode('utf-8'))
    j_order = request.get_json(silent=False, cache=False)
    if j_order is None:
        return jsonify({'result': False, 'message': 'Invalid data'})
    order = dict_to_model(Order, j_order, ignore_unknown=False)
    order.save(force_insert=True)
    for pos in order.positions:
        pos.save(force_insert=True)
    broker.publish_new_order(order)
    return jsonify(model_to_dict(order, backrefs=True))

@app.route('/orders/<int:order_id>', methods=['PUT'])
def update_order(order_id):
    try:
        order = Order.select().where(Order.id == order_id).get()
    except Order.DoesNotExist:
        return jsonify({'result': False, 'message': 'No order with specified id!'})
    j_order = request.get_json(silent=True, cache=False)
    if j_order is None:
        return jsonify({'result': False, 'message': 'Invalid data'})
    updated = update_model_from_dict(order, j_order, ignore_unknown=True)
    updated.save()
    broker.publish_updated_order(updated)
    return jsonify(model_to_dict(updated, backrefs=True))

@app.route('/orders/<int:order_id>', methods=['DELETE'])
def delete_order(order_id):
    try:
        Order.select().where(Order.id == order_id).get()
    except Order.DoesNotExist:
        return jsonify({'result': False, 'message': 'No order with specified id!'})
    Order.delete_by_id(order_id)
    broker.publish_deleted_order(order_id)

@app.route('/users', methods=['GET'])
def get_users():
    all_users = [model_to_dict(mod) for mod in User.select()]
    return jsonify(all_users)

@app.route('/users/<int:user_id>', methods=['GET'])
def get_user(user_id):
    try:
        user = User.select().where(User.id == user_id).get()
        return jsonify(model_to_dict(user))
    except User.DoesNotExist:
        return jsonify({'result': False, 'message': 'No user with specified id!'})