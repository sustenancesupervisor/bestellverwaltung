import os
import redis
from playhouse.shortcuts import (dict_to_model, model_to_dict,
                                 update_model_from_dict)
import json
from bestellverwaltung.database import Order, User, Food

class RedisBroker:

    def __init__(self, database, logger):
        self._redis = redis.StrictRedis.from_url(os.getenv('REDIS_URL', 'redis://localhost:6379'))
        self._pubsub = self._redis.pubsub()
        self._database = database
        self._logger = logger
    
    def start(self):
        self._logger.info('Starting message broker...')
        self._thread = self._pubsub.run_in_thread(sleep_time=0.001)
        self._logger.info(f'Thread is running: {self._thread.is_alive()}, ID: {self._thread.ident}, Native: {self._thread.native_id}')
    
    def stop(self):
        self._thread.stop()
    
    def subscribe_all(self):
        self._pubsub.subscribe(**{
            'user_created': self.on_user_create,
            'user_updated': self.on_user_updated,
            'user_deleted': self.on_user_deleted,
            'food_created': self.on_food_created,
            'food_updated': self.on_food_updated,
            'food_deleted': self.on_food_deleted
        })
    
    def on_user_create(self, message):
        self._logger.info("Created user: " + message['data'].decode('utf-8'))
        obj = json.loads(message['data'])
        user = dict_to_model(User, obj, ignore_unknown=True)
        user.save(force_insert=True)

    def on_user_updated(self, message):
        self._logger.info("Updated user: " + message['data'].decode('utf-8'))
        obj = json.loads(message['data'])
        user = dict_to_model(User, obj, ignore_unknown=True)
        user.save(force_insert=True)

    def on_user_deleted(self, message):
        pass

    def on_food_created(self, message):
        self._logger.info("Created food: " + message['data'].decode('utf-8'))
        obj = json.loads(message['data'])
        food = dict_to_model(Food, obj, ignore_unknown=True)
        food.save(force_insert=True)

    def on_food_updated(self, message):
        self._logger.info("Updated food: " + message['data'].decode('utf-8'))
        obj = json.loads(message['data'])
        food = dict_to_model(Food, obj, ignore_unknown=True)
        food.save(force_insert=True)

    def on_food_deleted(self, message):
        pass

    def publish_new_order(self, order):
        self._redis.publish('order_created', json.dumps(model_to_dict(order)))

    def publish_updated_order(self, order):
        self._redis.publish('order_updated', json.dumps(model_to_dict(order)))

    def publish_deleted_order(self, order_id):
        self._redis.publish('order_deleted', json.dumps({'id': order_id}))