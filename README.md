# Bestellverwaltung

## Bestellungen abrufen

**Alle Bestellungen**
```
curl -X GET http://bestellverwaltung:5000/orders
```

**Eine Bestellung**
```
curl -X GET http://bestellverwaltung:5000/orders/1
```

## Bestellung anlegen

```
curl -X POST -d "{\"user\": 1, \"positions\": [{\"food\": 1, \"quantity\": 1}]}" http://bestellverwaltung:5000/orders
```

## Bestellung aktualisieren

```
curl -X PUT -d "{\"user\": 1, \"positions\": [{\"food\": 1, \"quantity\": 1}]}" http://bestellverwaltung:5000/orders
```