build:
	docker build -t cozygalvinism/bestellverwaltung:latest -t registry.gitlab.com/sustenancesupervisor/bestellverwaltung:latest .

publish: build
	docker push registry.gitlab.com/sustenancesupervisor/bestellverwaltung:latest